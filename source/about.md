+++
title = "このブログについて"
date  = "2016-01-21T16:53:49+09:00"
type  = "about"
+++

# カラクリズムについて

このブログは、

  * [岡村 直樹 (にゃるら)](http://nyarla.net/)

が、少なくとも 10 年は続ける、という目標を掲げた、作品、著作、あるいは記録としてのブログです。

そして今まで、他の自分のブログでは、デタラメなどは書かないように心掛けていたけれども、
運営に対しては手を抜いていた、という点を改め、面と向かってブログというモノを書いてみよう、
という目標を掲げた存在でも有ります。

まあ、どこまで人としての道が続くかは運次第ですが、出来うる限り永く、
このブログを続けたいと思っています。

# 著作権

このブログの著作権は、それが他者の著作でない限り、

  * [岡村 直樹 (にゃるら)](http://nyarla.net/)

が所持しています。

なお、ブログ内検索を実現するコードについては、

  * [MIT-licese](http://nyarla.mit-license.org/2016)

の基に利用することが出来ます。

# このブログを実現するための利用物

このブログでは、その機能を実現するために、下記のソフトウェア等を利用しています:

  * 静的生成・コマンド実行
    * [Hugo](http://gohugo.io/) - ブログの静的生成
    * [Gulp](http://gulpjs.com/) - 各種タスクの実行
  * HTML 構造・スタイリング・機能の実装
    * [ampproject.org](https://github.com/ampproject/amphtml) - モバイル環境での閲覧の最適化 ([Apache 2.0](https://github.com/ampproject/amphtml/blob/master/LICENSE))
    * [normalize.css](https://necolas.github.io/normalize.css/) - スタイルの初期化 ([MIT](https://github.com/necolas/normalize.css/blob/master/LICENSE.md))
  * ホスティング
    * [AWS S3](https://aws.amazon.com/jp/s3/) - ブログ公開のためのホスティング
    * [KeyCDN](https://www.keycdn.com/) - CDN・http/2 + Let's Encryt への対応
    * [ムームードメイン](https://muumuu-domain.com/) - ドメイン管理
    * [Github](https://github.com/) - ソースファイルの管理、及びホスティング
    * [wercker.com](http://wercker.com/) - ブログの自動デプロイ

# 広告

このブログでは、かつて Google Adsense を広告として表示していましたが、
ドメイン移転に伴う改修の際、作り上げたスタイリングを損なう、と判断したため、取り払いました。

そのため、2016 年 4月現在、特に広告の類は掲載していません。


# 訪問者の追跡

このブログでは、訪問者のアクセス情報の解析、及び、このブログへのアクセス数の正確な把握のために、

  * [Google Analytics](https://www.google.co.jp/intl/ja/analytics/)

によるアクセス解析を行なっています。

そのため、このブログへの訪問者のアクセス情報については、
その利用サービスの特性上、上記利用サービス開発元の、Google 社と共有状態にあります。

また、今後、このブログを運営する中で、情報解析の委託等で、自分と訪問者以外の第三者する場合、
事前にブログのエントリとして告知した上で、アクセス情報を共有する場合が有り得ますので、その点、予めご了承下さい。

なお、収集した情報については、このブログの運営の参考にする以外に用ずることは無く、
それ以外の用途でも使用しない、と宣言致します。

# 連絡先

このブログについて、筆者である岡村 直樹に連絡を取りたい場合には、

  * Twitter: [@nyarla](https://twitter.com/nyarla)
  * Email: <nyarla@thotep.net>

まで、ご連絡下さい。

なお、 Email や Twitter 経由で連絡を頂いたとしても、
その時々の体調や、あるいは、その内容の如何により、
場合によっては返信等の応答を行なわない場合も有りますので、
その点、予めご了承下さい。

