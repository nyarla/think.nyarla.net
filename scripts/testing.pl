#!/usr/bin/env perl

use strict;
use warnings;

use Test::More;
use File::Spec;
use File::Basename;

my $basedir = File::Spec->rel2abs( File::Spec->catdir(dirname(__FILE__), "../public"));

subtest 'checking for files are not empty' => sub {
  my @files = `find ${basedir} -type f -name '*.*'`;

  for my $file (sort @files) {
    chomp($file);

    ok( ! -z $file, "${file} is not empty" );
  }
};

subtest 'checking for removed files does not exist' => sub {
  my @paths = qw[
    post/index.html
    post/index.xml
  ];

  for my $path (sort @paths) {
    my $fullpath = "${basedir}/${path}";

    ok( ! -e "${fullpath}", "${fullpath} does not exist" );
  }
};

done_testing;

