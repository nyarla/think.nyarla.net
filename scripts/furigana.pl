#!/usr/bin/env perl

use strict;
use warnings;

use Encode;

my $RE = qr< [{] ( [^\|]+ (?:\|[^\|\}]+)+) [}] >x;

sub markup {
  my $src = shift @_;
  my @src = split m{\|}, $src;

  my $base = shift @src;
  my @base = split m{}, $base;
  my @text = @src;

  my $data = q{};

  if ( scalar(@base) == scalar(@text) ) {
    for my $idx ( 0 .. scalar(@text) -1 ) {
      $data .= $base[$idx] . '<rt>' . $text[$idx] . '</rt>';
    }
  } else {
    $data .= join(q{}, @base) . '<rt>' . join(q{}, @text) . '</rt>';
  }

  return "<ruby>${data}</ruby>";
}

sub process {
  my $text = Encode::decode_utf8(shift @_);

  $text =~ s<$RE><markup($1)>ge;

  return Encode::encode_utf8($text);
}

sub main {
  while (defined(my $line = <STDIN>)) {
    print process($line);
  }
}

sub testing {
  require Test::More; Test::More->import;

  is(
    process('{電子書籍|でん|し|しょ|せき}'),
    '<ruby>電<rt>でん</rt>子<rt>し</rt>書<rt>しょ</rt>籍<rt>せき</rt></ruby>',
  );

  is(
    process('{約束されし勝利の剣|エクスカリバー}'),
    '<ruby>約束されし勝利の剣<rt>エクスカリバー</rt></ruby>',
  );

  done_testing();
}

if ( ! $ENV{'HARNESS_ACTIVE'} ) {
  main();
} else {
  testing();
}
