all: clean preprocess build postprocess

clean:
	rm -rf public/
	rm -rf content/
	git checkout content/

preprocess:
	find source -type d \
		| sed 's!source/!!' \
		| grep -v -E '^source$$' \
		| xargs -n1 -I{} sh -c 'mkdir -p "content/{}"'
	find source -type f -name '*.md' \
		| sed 's!source/!!' \
		| xargs -n1 -I{} sh -c 'cat "source/{}" | perl scripts/furigana.pl > "content/{}"'

build:
	hugo -v

postprocess:
	rm -rf public/post/index.html
	rm -rf public/post/index.xml

dev: clean preprocess
	hugo serve --disableLiveReload=true || make clean

deploy:
	s3cmd sync --exclude="*.DS_Store" --acl-public --delete-removed --verbose public/ s3://think.nyarla.net/

test: all
	prove scripts/testing.pl

wercker: wercker-build wercker-deploy wercker-clean

wercker-clean:
	rm -rf _*/

wercker-build:
	wercker --environment .env build

wercker-deploy:
	wercker --environment .env deploy
